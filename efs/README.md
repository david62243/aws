### Elastic File System 

**NOTE:** Recommend using AWS Budgets to manage costs


https://aws.amazon.com/efs/


Move infrequent Access to save $$$ 

Pricing
Standard storage	$0.30 per GB
Standard-Infrequent Access storage	$0.025 per GB
One Zone storage	$0.160 per GB
One Zone-Infrequent Access storage	$0.0133 per GB
Infrequent Access requests	$0.010 per GB transferred
Provisioned Throughput	$6.00 per MB/s


One Zone or Regional (Zone is less expensive) 


#### Instructions to add efs Support

https://docs.aws.amazon.com/efs/latest/ug/installing-amazon-efs-utils.html


### Mount 


mkdir efs

sudo mount -t efs -o tls fs-0fdb7f74:/ /var/www/html/media



fs-0fdb7f74:/ /var/www/html/media efs _netdev,tls 0 0


The instance has to be in same subnet as the efs

Using an Security group that allows all traffic for both Autoscale Group and EFS allowed the volume to mount.  




### Create Image

Create Instance (my account)
- Ubuntu 20.04 
- t2.micro
- Name: nginx 
- For one zone EFS instance must be in same zone as EFS (e.g. us-east-2b)
- Instance should have SSG with EFS (e.g. default with full traffic)

```
sudo apt update
sudo apt upgrade -y 
sudo apt install nginx -y
```

### EFS 

```
sudo apt-get -y install git binutils
```

```
git clone https://github.com/aws/efs-utils
```

```
cd efs-utils/
./build-deb.sh
sudo apt-get -y install ./build/amazon-efs-utils*deb
```

```
sudo su - 
cd /var/www/html/
mv index.nginx-debian.html index.nginx-debian.html.orig
echo "This is the index" > index.html
mkdir files
```

```
sudo mount -t efs -o tls fs-ac8420d7:/ /var/www/html/files
```

Edit /etc/fstab  (sudo vi /etc/fstab)

Add line (e.g.)
```
fs-ac8420d7:/ /var/www/html/files efs _netdev,tls 0 0
```

Test
```
sudo mount -a 
```

Create file(s) in /var/www/html/media 

Test 
```
sudo reboot
```

Node should restart and curl should still work.  

### Create Image 

From AWS Create Image.  (AMI)

The Image had
- nginx installed and running
- efs configured and attached

### Launch Configuration 

Uses the AMI

Specify Image Type:  t2.micro

Security Group (Include the one used for EFS) 

### Create Auto Scale Group 

Select the Launch Configuration

Use Same VPC / Subnet as EFS 

Use Same Subnet as specified in EFS for single zone EFS Configuration. 

### Load Balancer 

Create New Load Balancer or Use Existing (Used one I created earlier pointing to the Image EC2 Instance) 




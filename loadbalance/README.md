## AWS Load Balancer 


### Create EC2 Instance

Type: t2.micro 
OS: Ubuntu 20.04 

750 hours for free / month.  

webserver1 and webserver2.  

### Install Apache Web Servers

https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-ubuntu-20-04


```
cat /var/www/html/index.html
This is webserver1
```

This is webserver2 on second web server. 

### Created Target Group 

This points to Web Servers (Can be just port 80).   SSL Offloading.  

### Created Domain (Route 53)

Registered Domain jennings.solution ($12/year) 

Route 53 costs $0.50/month 


### Create Load Balancer 

http (port 80) was simple.  

https (port 443) more complicated. 

Self-Signed cert (not trusted by browsers)

Tried https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-ubuntu-20-04 
- Let's encrypt won't issue certs for AWS names 

**NOTE:** Don't use Classic Load Balancer.  Retiring 2022 

Application Load Balancer 
- Internet Facing 
- Network: Same vpc as the web servers 
- SSG's:  One that allows 80 and 443 from anywhere also included default ssg (don't think this is needed)  
- Listeners/Routing 
  - Route http to https 
  - Route https to Target
- SSL Cert From ACM (Requested Certificate for www.jennings.solutios);  Created Record in Route 53 

Added A Record for www.jennings.solutions in Route 53 connect to the Load Balancer 

### Load Balancing 

If both web servers are up.  Traffic appears to round robin to the two backends.  If one goes down all traffic goes the server that is still running. 



